const EmlParser = require('eml-parser');
const { stringify } = require('csv-stringify');
const fs = require('fs');

const sourcePath = __dirname + "\\messages\\";
const outputPath = __dirname + "\\data\\";

handleAttachment = (subject, attach, file) => {
    stringify(Array.of({
        File: file,
        Subject: subject,
        Name: attach.filename,
        Body: outputPath + attach.filename
    }), (err, output) => {
        fs.writeFile(outputPath + "Attachments.csv", output, { 'flag': 'a' }, (err) => {
            if (err) console.log(err);
        });
        if (err) console.log(err);
    });
    fs.writeFile(outputPath + attach.filename, attach.content, (err) => {
        if (err) console.log(err);
        else {
            console.log(`File ${attach.filename} was saved.\n`);
        }
    });
}

fs.readdir(sourcePath, (err, files) => {
    files.forEach((file, i) => {
        let emailFile = fs.createReadStream(sourcePath + file);
        new EmlParser(emailFile)
            .parseEml()
            .then(data => {
                stringify(Array.of({
                    File: file,
                    Subject: data.subject,
                    SuppliedName: data.from.value[0].name,
                    SuppliedEmail: data.from.value[0].address,
                    Description: data.text,
                    Origin: "Email",
                    RecordType: "0124H000000kJIlQAM",
                    Status: "Открыто",
                    CaseOwner: "00G4H000005YpG0UAK",
                    Priority: "Low",
                    Type: "Consultation"
                }), (err, output) => {
                    fs.writeFile(outputPath + "Emails.csv", output, { 'flag': 'a' }, (err) => {
                        if (err) console.log(err);
                    });
                    if (err) console.log(err);
                });
                data.attachments.forEach(attach => {
                    handleAttachment(data.subject, attach, file);
                });
            })
            .catch(err => {
                console.log(err);
            })
    });
});
